var mongoose = require('mongoose');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
var saltRounds = 10;
const uniqueValidator = require('mongoose-unique-validator');

var Token = require('./token');
const mailer = require('../mailer/mailer');
var crypto = require('crypto');

const validateEmail = function (email) {
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(email);
};

var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'Por favor ingrese un email valido'],
        match: [/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/]
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio'],
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    }
});

usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} ya existe con otro usuario' });

usuarioSchema.pre('save', function (next) {
    if (this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.statics.add = function (aUser, cb) {
    return this.create(aUser, cb);
};

usuarioSchema.statics.findByEmail = function (aEmail, cb) {
    return this.findOne({ email: aEmail }, cb);
};

usuarioSchema.statics.update = function (aUser, cb) {
    return this.updateOne({ email: aUser.email }, aUser, cb);
};

usuarioSchema.methods.validaPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

usuarioSchema.methods.reservar = function (biciId, desde, hasta, cb) {
    var reservas = new Reserva({ usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta });
    console.log(reservas);
    reservas.save(cb);
};

usuarioSchema.methods.enviarEmailBienvenida = function (cb) {
    const token = new Token({ _userId: this.id,
                                token: crypto.randomBytes(16).toString("hex")
                            });
    const emailDestination = this.email;

    token.save(function (error) {
        if (error) {
            return console.log(error.message);
        }
        const mailOption = {
            from: 'no-reply@redbicicletas.com',
            to: emailDestination,
            subject: 'Verificacion de cuenta',
            text: 'Hola \n\n' +
                'Por favor, para verificar su cuenta haga click en el siguiente link:\n\n' +
                'http://localhost:3000' + '/token/confirmacion/' + token.token + '\n'
        };

        mailer.sendMail(mailOption, function (error) {
            if (error) {
                return console.log(error.message);
            }

            console.log('Se ha enviado correo de verifiacion a:' + emailDestination);
        });
    });
}

module.exports = mongoose.model('Usuario', usuarioSchema);