# RED BICICLETAS
## Desarrollo del lado servidor: NodeJS, Express y MongoDB
### **Semana 1**

* Conceptos básicos de NodeJS.
* Implementación de servidor JS con Express.
* Arquitectura MVC.
* Introducción a APIs.

### **Semana 2** 

* Testing con Jasmine.
* MongoDB con Mongoose como ODM.

### **Semana 3**

* Registrar usuarios y autorizarlos.
* Conocer diferentes formas de autorización utilizando Passport.
* Segurizar una API Rest utilizando JWT.

Alumno: Gonzalo Martín

