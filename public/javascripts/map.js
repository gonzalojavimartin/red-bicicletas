var map = L.map('main_map').setView([-34.6538875,-58.575659], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'copy: <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

$.ajax({
    dataType: 'json',
    url: 'api/bicicletas',
    success: function (response) {
        response.bicicletas.forEach(function (bici) {
            L.marker([bici.ubicacion[0], bici.ubicacion[1]], { title: bici.id }).addTo(map);
        });
    }
});