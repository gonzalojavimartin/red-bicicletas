var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

beforeEach(() => {
    Bicicleta.allBicis = [];
});

describe('Bicicleta API', () => {
    describe('GET Bicicletas /', () => {
        it ('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);
            var biciA = new Bicicleta(1, 'Rojo', 'ModeloA', [-34.6538875, -58.575659]);
            Bicicleta.add(biciA);
            request.get('http://localhost:3000/api/bicicletas', function (error, response, body) {
               expect(response.statusCode).toBe(200);
            });
        });
    });

    describe('POST Bicicletas /create', () => {
        it ('Status 200', (done) => {
            var headers = { 'content-type' : 'application/json' };
            var biciA = '{ "id" : 10, "color" : "Rojo", "modelo" : "ModeloRoja", "lat" : -34.6538875, "lng" : -58.575659 }';
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: biciA
            }, {},function (error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe('Rojo');
                done();
            });
        });
    });

    describe('POST Bicicletas /update', () => {
        it ('Status 200', (done) => {
            var bici = new Bicicleta(1, 'Rojo', 'ModeloA', [-34.6538875, -58.575659])
            Bicicleta.add(bici);

            var headers = { 'content-type' : 'application/json' };
            var biciA = '{ "id" : 1, "color" : "Azul", "modelo" : "Modelo Roja", "lat" : -34.6538875, "lng" : -58.575659 }';
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/update',
                body: biciA
            }, {},function (error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(1).color).toBe('Azul');
                done();
            });
        });
    });

    describe('POST Bicicletas /delete', () => {
        it ('Status 200', (done) => {
            var bici = new Bicicleta(1, 'Rojo', 'ModeloA', [-34.6538875, -58.575659])
            Bicicleta.add(bici);

            var headers = { 'content-type' : 'application/json' };
            var biciA = '{ "id" : 1 }';
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/delete',
                body: biciA
            }, {},function (error, response, body) {
                expect(response.statusCode).toBe(204);
                expect(Bicicleta.allBicis.length).toBe(0);
                done();
            });
        });
    });
});