var mongoose = require('mongoose');
var Bicicleta =  require('../../models/bicicleta');
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');

describe('Testing Usuarios', function(){
    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/test';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function () {
            console.log('Conectado a la db de test!');
            done();
        });
    });

    afterEach(function (done) {
        Reserva.deleteMany({},function(err, success){
            if(err) console.log(err);
            Usuario.deleteMany({}, function(err, success){
                if(err) console.log(err);
                Bicicleta.deleteMany({}, function (err, success) {
                    if (err) console.log(err);
                    done();
                });
            });
        });
    });

    describe('Cuando un Usuario reserva una bici', () => {
        it('debe existir la reserva', (done) => {
            const usuario = new Usuario({ nombre: 'Gonzalo Martin' });
            usuario.save();
            const bicicleta = new Bicicleta({ code: 1, color: "Rojo", modelo: "Modelo rojo" });
            bicicleta.save();

            var today = new Date();
            var tomorrow = new Date();
            tomorrow.setDate(today.getDate() +1);
            usuario.reservar(bicicleta.id, today, tomorrow, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reserva){
                    console.log(reserva[0]);
                    expect(reserva.length).toBe(1);
                    expect(reserva[0].diaDeReservas()).toBe(2);
                    expect(reserva[0].bicicleta.code).toBe(1);
                    expect(reserva[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });
});