var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function () {
    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/test';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function () {
           console.log('Conectado a la db de test!');
           done();
        });
    });

    afterEach(function (done) {
        Bicicleta.deleteMany({}, function (error, success) {
            if (error) console.error(error);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', function (done) {
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', function (done) {
            Bicicleta.allBicis(function (error, bicis) {
                if (error) console.error(error);
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('agrega solo una bici', function (done) {
            var aBici = new Bicicleta({
                code: 10,
                color: 'Rojo',
                modelo: 'Modelo rojo'
            });
            Bicicleta.add(aBici, function (error, newBici) {
                if (error) console.error(error);
                Bicicleta.allBicis(function (error, bicis) {
                    if (error) console.error(error);
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toBe(10);
                    done();
                });
            });
        });
    });
    describe('Bicicleta.findByCode', () => {
        it('debe devolver la bici con code 11', function (done) {
            var aBici1 = new Bicicleta({ code: 10, color: 'Rojo', modelo: 'Modelo rojo' });
            Bicicleta.add(aBici1, function (error, newBici) {
                if (error) console.error(error);
                var aBici2 = new Bicicleta({ code: 11, color: 'Azul', modelo: 'Modelo Azul' });
                Bicicleta.add(aBici2, function (error, newBici) {
                    if (error) console.error(error);
                    Bicicleta.findByCode(11, function (error, targetBici) {
                        if (error) console.error(error);
                        expect(targetBici.code).toBe(11);
                        expect(targetBici.color).toBe(aBici2.color);
                        expect(targetBici.modelo).toBe(aBici2.modelo);
                        done();
                    });
                });
            });
        });
    });
    describe('Bicicleta.findByCode', () => {
        it('debe eliminar la bici con code 11', function (done) {
            var aBici1 = new Bicicleta({ code: 10, color: 'Rojo', modelo: 'Modelo rojo' });
            Bicicleta.add(aBici1, function (error, newBici) {
                if (error) console.error(error);
                var aBici2 = new Bicicleta({ code: 11, color: 'Azul', modelo: 'Modelo Azul' });
                Bicicleta.add(aBici2, function (error, newBici) {
                    if (error) console.error(error);
                    Bicicleta.removeByCode(11, function (error, r) {
                        if (error) console.error(error);
                        Bicicleta.allBicis(function (error, bicis) {
                            if (error) console.error(error);
                            expect(bicis.length).toBe(1);
                            expect(bicis[0].code).toBe(10);
                            done();
                        });
                    });
                });
            });
        });
    });
});