var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function (req, res) {
    Bicicleta.allBicis(function (err, targetBicis) {
        if (err) console.log(err);
        res.render('bicicletas/index', { bicis : targetBicis });
    })
}

exports.bicicleta_create_get = function (req, res){
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function (req, res){
    var ubicacion = [req.body.lat, req.body.lng];
    var bici = new Bicicleta({ code: req.body.id, color: req.body.color, modelo: req.body.modelo, ubicacion: ubicacion});
    Bicicleta.add(bici, function (err, newBici) {
        if (err) console.log(err);
    });
    res.redirect('/bicicletas');
}

exports.bicicleta_update_get = function (req, res){
    Bicicleta.findByCode(req.params.code, function (error, targetBici) {
        if (error) console.log(err);
        res.render('bicicletas/update', { bici: targetBici });
    });
}

exports.bicicleta_update_post = function (req, res){
    Bicicleta.findByCode(req.params.code, function (error, targetBici) {
        if (error) console.log(error);
        targetBici.code = req.body.code;
        targetBici.color = req.body.color;
        targetBici.modelo = req.body.modelo;
        targetBici.ubicacion = [req.body.lat, req.body.lng];
        Bicicleta.update(targetBici, function (error, result) {
            if (error) console.log(error);
            res.redirect('/bicicletas');
        });
    });
}

exports.bicicleta_delete_post = function(req, res){
    Bicicleta.removeByCode(req.body.code);
    res.redirect('/bicicletas');
}